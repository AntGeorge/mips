.data
	board:
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
	
	found: .asciiz "Found solution!"
	not_found_msg: .asciiz ":("
	space: .asciiz " "
	new_line: .asciiz "\n"
.text

# s0: q1
# s1: q2
# ...
# s7: q8

li $s0, 0

queen0_loop:
	li $s1, 8
	bge $s0, 8, EXIT
	sb $zero, board($s0)

	queen1_loop:
		li $s2, 16
		bge $s1, 16, queen1_loop_end
		sb $zero, board($s1)

		queen2_loop:
			li $s3, 24
			bge $s2, 24, queen2_loop_end
			sb $zero, board($s2)

			queen3_loop:
				li $s4, 32
				bge $s3, 32, queen3_loop_end
				sb $zero, board($s3)

				queen4_loop:
					li $s5, 40
					bge $s4, 40, queen4_loop_end
					sb $zero, board($s4)

					queen5_loop:
						li $s6, 48
						bge $s5, 48, queen5_loop_end
						sb $zero, board($s5)

						queen6_loop:
							li $s7, 56
							bge $s6, 56, queen6_loop_end
							sb $zero, board($s6)
							
							queen7_loop:
								bge $s7, 64, queen7_loop_end
								sb $zero, board($s7)
								
								jal check_solve
								bnez $v0, not_found
								jal solution_found

								not_found:							
								li $t0, 1
								sb $t0, board($s7)
								addi $s7, $s7, 1
								j queen7_loop

							queen7_loop_end:
								li $t0, 1
								sb $t0, board($s6)
								addi $s6, $s6, 1
								j queen6_loop

						queen6_loop_end:
							li $t0, 1
							sb $t0, board($s5)
							addi $s5, $s5, 1
							j queen5_loop

					queen5_loop_end:
						li $t0, 1
						sb $t0, board($s4)
						addi $s4, $s4, 1
						j queen4_loop

				queen4_loop_end:
					li $t0, 1
					sb $t0, board($s3)
					addi $s3, $s3, 1
					j queen3_loop

			queen3_loop_end:
				li $t0, 1
				sb $t0, board($s2)
				addi $s2, $s2, 1
				j queen2_loop

		queen2_loop_end:
			li $t0, 1
			sb $t0, board($s1)
			addi $s1, $s1, 1
			j queen1_loop

	queen1_loop_end:
		li $t0, 1
		sb $t0, board($s0)
		addi $s0, $s0, 1
		j queen0_loop




solution_found:
	li $v0, 4
	la $a0, found
	syscall
	
	li $v0, 1
	subi $a0, $s0, 0
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	subi $a0, $s1, 8
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	subi $a0, $s2, 16
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	subi $a0, $s3, 24
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	subi $a0, $s4, 32
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	subi $a0, $s5, 40
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	subi $a0, $s6, 48
	syscall
	
	li $v0, 4
	la $a0, space
	syscall
	
	li $v0, 1
	subi $a0, $s7, 56
	syscall

	li $v0, 4
	la $a0, new_line
	syscall

	jr $ra

EXIT:
	li $v0, 4
	la $a0, not_found_msg
	li $v0, 10
	syscall

# Ελένχει αν η λύση είναι σωστή.
check_solve:
	la $t0, board # counter
	la $t5, board # Σταθερά
	
	check_solve_loop:
		
		addi $t0, $t0, 1
		lb $t9, -1($t0)
		sub $t8, $t0, $t5
		bge $t8, 64, check_solve_exit
		bnez $t9, check_solve_loop
		addi $t0, $t0, -1
		
		la $a0, board
		li $a1, 8
		sub $a0, $t0, $a0
		div $a0, $a1
		mflo $a0
		mfhi $a1
		
		############## Έλεγχος στήλης ##############
		li $t1, 0
		move $t2, $a1
		check_solve_loop_column:			
			lb $t3, board($t2)
			add $t1, $t1, $t3
			addi $t2, $t2, 8
			blt $t2, 64, check_solve_loop_column
			bne $t1, 7, check_solve_wrong
		############################################
		
		############ Έλεγχος γραμμής ############
		li $t1, 0
		sll $t2, $a0, 3
		addi $t9, $t2, 8
		check_solve_loop_line:		
			lb $t3, board($t2)
			add $t1, $t1, $t3
			addi $t2, $t2, 1
			blt $t2, $t9, check_solve_loop_line
			bne $t1, 7, check_solve_wrong
		#########################################
		
		########### Έλεγχος \ διαγώνιας ###########
		li $t1, 0
		li $t8, 0
		
		sub $t6, $a0, $a1
		bgez $t6, check_solve_left_bottom
	
		sub $t6, $a1, $a0
		add $t6, $t6, $t5 # Έναρξη διαγωνίου
	
		li $t7, 7
		sub $t7, $t7, $a1
		add $t7, $t7, $a0
		sll $t7, $t7, 3
		addi $t7, $t7, 7
		add $t7, $t7, $t5 # Τέλος διαγωνίου
		
		j check_solve_final
	
		check_solve_left_bottom:
			sll $t6, $t6, 3
			add $t6, $t6, $t5 # Έναρξη διαγωνίου
		
			li $t7, 7
			sub $t7, $t7, $a0
			add $t7, $t7, $a1
			addi $t7, $t7, 56
			add $t7, $t7, $t5 # Τέλος διαγωνίου
		
		check_solve_final:
			addi $t8, $t8, 1
			beq $t6, $t7, check_solve_final_exit
			lb $t3, 0($t6)
			add $t1, $t1, $t3
			addi $t6, $t6, 9
			j check_solve_final
		
		check_solve_final_exit:
			lb $t3, 0($t6)
			add $t1, $t1, $t3
		
		addi $t8, $t8, -1
		bne $t1, $t8, check_solve_wrong
		###########################################
		
		########### Έλεγχος / διαγώνιας ###########
		li $t1, 0
		li $t8, 0
		
		add $t6, $a0, $a1
		ble $t6, 7, check_solve_left_top
	
		addi $t6, $t6, -7
		sll $t6, $t6, 3
		addi $t6, $t6, 7
		add $t6, $t6, $t5 # Έναρξη διαγωνίου
	
		add $t7, $a0, $a1
		addi $t7, $t7, -7
		addi $t7, $t7, 56
		add $t7, $t7, $t5 # Τέλος διαγωνίου	
	
		j check_solve_final2
	
		check_solve_left_top:
			add $t6, $t6, $t5 # Έναρξη διαγωνίου
		
			add $t7, $a0, $a1
			sll $t7, $t7, 3
			add $t7, $t7, $t5 # Τέλος διαγωνίου		
		
		check_solve_final2:
			addi $t8, $t8, 1
			beq $t6, $t7, check_solve_final2_exit
			lb $t3, 0($t6)
			add $t1, $t1, $t3
			addi $t6, $t6, 7
			j check_solve_final2
	
		check_solve_final2_exit:
			lb $t3, 0($t6)
			add $t1, $t1, $t3
		
		addi $t8, $t8, -1
		bne $t1, $t8, check_solve_wrong
		###########################################
	
	addi $t0, $t0, 1
	sub $t9, $t0, $t5
	blt $t9, 64, check_solve_loop
	
	check_solve_exit:
	li $v0, 0
	jr $ra
	
	check_solve_wrong:
		li $v0, -1
		jr $ra
		
		
