.data
	board:
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
	moves:
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
		.byte 1, 1, 1, 1, 1, 1, 1, 1
	
	first_move_msg: .asciiz "===Give me the first move===\n"
	start_msg: .asciiz "\nThe solution is (x,y)...\n"
	x_msg: .asciiz "x: "
	y_msg: .asciiz "y: "
	new_line: .asciiz "\n"
	comma: .asciiz ","
.text

# Άδεια θέση: 1
# Βασίλισσα: -1
# Κλειστή Θέση: 0

la $a0, first_move_msg
li $v0, 4
syscall

la $a0, x_msg
li $v0, 4
syscall

li $v0, 5
syscall
li $t0, 0

la $a0, y_msg
li $v0, 4
syscall

li $v0, 5
syscall
li $t1, 0

la $a0, start_msg
li $v0, 4
syscall


Solve:
	move $a0, $t0
	move $a1, $t1
	jal make_move

	jal find_best_move
	move $t0, $v0
	move $t1, $v1
	
	blt $v0, $zero, end_solve
	
	# Τυπώνει την κίνηση που έκανε.
	move $a0, $t0
	li $v0, 1
	syscall
	
	la $a0, comma
	li $v0, 4
	syscall
	
	move $a0, $t1
	li $v0, 1
	syscall
	
	la $a0, new_line
	li $v0, 4
	syscall
	
	j Solve

end_solve:


# EXIT
li $v0, 10
syscall

# Επιστρέφει στους καταχωρητές $v0,$v1 τις συντεταγμένες τις καλύτερης κίνησης.
find_best_move:
	addi $sp, $sp, 16
	sw $s0, 0($sp)
	sw $s1, -4($sp)
	sw $s2, -8($sp)
	sw $ra, -12($sp)
	
	la $t0, board
	li $t4, 64
	
	# Αρχική κίνηση.
	li $s0, -1
	li $s1, -1
	
	# Αρχικό penalty
	li $s2, 65
	
	find_best_move_loop:
		lb $t3, 0($t0)
		
		beqz $t3, not_possible
		
		la $t1, board
		sub $t3, $t0, $t1
		
		li $t1, 8
		div $t3, $t1
		mflo $a0
		mfhi $a1
		
		addi $sp, $sp, 8
		sw $t0, 0($sp)
		sw $t4, -4($sp)
		
		jal penalty_of_move
		
		lw $t0, 0($sp)
		lw $t4, -4($sp)
		addi $sp, $sp, -8
		
		bge $v0, $s2, not_possible
		
		move $s0, $a0
		move $s1, $a1
		move $s2, $v0
		
		not_possible:
			addi $t0, $t0, 1
			addi $t4, $t4, -1
			bnez $t4, find_best_move_loop
		
	move $v0, $s0
	move $v1, $s1
	
	lw $s0, 0($sp)
	lw $s1, -4($sp)
	lw $s2, -8($sp)
	lw $ra, -12($sp)
	addi $sp, $sp, -16
	jr $ra
	

# Επιστρέφει στον $v0 την ποινή της κίνησης στο ($a0, $a1).
# Δηλαδή πόσα ελεύθερα τετράγωνα θα κλείσει αυτή η κίνηση.
# Η καλύτερη κίνηση είναι αυτή με το μικρότερο penalty.
penalty_of_move:
	addi $sp, $sp, 8
	sw $ra, -4($sp)
	sw $s0, 0($sp)	

	li $s0, 0 # Συνολικό penalty

	########## Penalty στήλης ##########
	move $t2, $a1
	penalty_column:			
		lb $t3, board($t2)
		add $s0, $s0, $t3
		addi $t2, $t2, 8
		blt $t2, 64, penalty_column
	####################################
	
	######### Penalty γραμμής #########
	sll $t2, $a0, 3
	addi $t1, $t2, 8
	penalty_line:		
		lb $t3, board($t2)
		add $s0, $s0, $t3
		addi $t2, $t2, 1
		blt $t2, $t1, penalty_line
	###################################
	
	########## Penalty \ διαγωνίου ##########
	jal left_to_right
	move $t4, $v0
	move $t5, $v1
	penalty_diagonial1:
		lb $t3, 0($t4)
		add $s0, $s0, $t3
		addi $t4, $t4, 9
		ble $t4, $t5, penalty_diagonial1
	#########################################
	
	####### Penalty / διαγωνίου #######
	jal right_to_left
	move $t4, $v0
	move $t5, $v1
	penalty_diagonial2:
		lb $t3, 0($t4)
		add $s0, $s0, $t3
		addi $t4, $t4, 7
		ble $t4, $t5, penalty_diagonial2
	###################################

	move $v0, $s0
	lw $s0, 0($sp)
	lw $ra, -4($sp)
	addi $sp, $sp, -8
	jr $ra

# Τοποθέτηση βασίλισσας στο ($a0,$a1) και κλείσιμο των θέσεων που απειλεί.
# Δεν γίνετε κάποιος έλεγχος αν αυτή η κίνηση είναι νόμιμη ούτε αν υπάρχει
# ήδη εκεί μια άλλη βασίλισσα.
make_move:
	addi $sp, $sp, 4
	sw $ra, 0($sp)

	la $t5, board
	
	# Κλείσιμο στήλης #
	add $t1, $a1, $t5
	sb $zero, 0($t1)
	sb $zero, 8($t1)
	sb $zero, 16($t1)
	sb $zero, 24($t1)
	sb $zero, 32($t1)
	sb $zero, 40($t1)
	sb $zero, 48($t1)
	sb $zero, 56($t1)
	###################
	
	# Κλείσιμο γραμμής #
	sll $t1, $a0, 3
	add $t1, $t1, $t5
	sb $zero, 0($t1)
	sb $zero, 1($t1)
	sb $zero, 2($t1)
	sb $zero, 3($t1)
	sb $zero, 4($t1)
	sb $zero, 5($t1)
	sb $zero, 6($t1)
	sb $zero, 7($t1)
	####################

	# Κλείσιμο \ διαγώνιας #
	jal left_to_right
	move $t0, $v0
	move $t1, $v1
		
	make_move_final:
		beq $t0, $t1, make_move_final_exit
		sb $zero, 0($t0)
		addi $t0, $t0, 9
		j make_move_final
	
	make_move_final_exit:
		sb $zero, 0($t0)
	########################
	
	# Κλείσιμο / διαγώνιας #
	jal right_to_left
	move $t0, $v0
	move $t1, $v1

	make_move_final2:
		beq $t0, $t1, make_move_final2_exit
		sb $zero, 0($t0)
		addi $t0, $t0, 7
		j make_move_final2
	
	make_move_final2_exit:
		sb $zero, 0($t0)
	########################
	
	sll $t1, $a0, 3
	add $t1, $t1, $a1
	sb $zero, board($t1)
	sb $zero, moves($t1)

	lw $ra, 0($sp)
	addi $sp, $sp, -4
	jr $ra

# Επιστρέφει την αρχή και το τέλος της διαγωνίου από αριστερά προς τα
# δεξιά. Στο $v0 είναι η αρχή και στο $v1 είναι το τέλος.
# Στα $a0 και $a1 είναι οι συντεταγμένες εισόδου.
left_to_right:
	la $t5, board # Σταθερά
	
	sub $t0, $a0, $a1
	bgez $t0, left_bottom
	
	sub $t0, $a1, $a0
	add $t0, $t0, $t5 # Έναρξη διαγωνίου
	
	li $t1, 7
	sub $t1, $t1, $a1
	add $t1, $t1, $a0
	sll $t1, $t1, 3
	addi $t1, $t1, 7
	add $t1, $t1, $t5 # Τέλος διαγωνίου
	
	j final
	
	left_bottom:
		sll $t0, $t0, 3
		add $t0, $t0, $t5 # Έναρξη διαγωνίου
		
		li $t1, 7
		sub $t1, $t1, $a0
		add $t1, $t1, $a1
		addi $t1, $t1, 56
		add $t1, $t1, $t5 # Τέλος διαγωνίου
		
	final:
		move $v0, $t0
		move $v1, $t1
		jr $ra

# Ίδια φάση με το left_to_right αλλά αυτό επιστρέφει την διαγώνιο από
# δεξιά προς τα αριστερά.
right_to_left:
	la $t5, board # Σταθερά
	add $t0, $a0, $a1
	ble $t0, 7, left_top
	
	addi $t0, $t0, -7
	sll $t0, $t0, 3
	addi $t0, $t0, 7
	add $t0, $t0, $t5 # Έναρξη διαγωνίου
	
	add $t1, $a0, $a1
	addi $t1, $t1, -7
	addi $t1, $t1, 56
	add $t1, $t1, $t5 # Τέλος διαγωνίου	
	
	j final2
	
	left_top:
		add $t0, $t0, $t5 # Έναρξη διαγωνίου
		
		add $t1, $a0, $a1
		sll $t1, $t1, 3
		add $t1, $t1, $t5 # Τέλος διαγωνίου

	final2:
		move $v0, $t0
		move $v1, $t1
		jr $ra

# Επαναφορά σκακιέρας στις default τιμές (1).
clear_board:
	li $t0, 0	# Address of board
	li $t1, 64	# Size of board
	li $t2, 1	# Default value of board
	clear_board_loop:
		sb $t2, board($t0)
		sb $t2, moves($t0)
		addi $t0, $t0, 1
		addi $t1, $t1, -1
		bnez $t1, clear_board_loop
	jr $ra

# Επιστρέφει όλες τις κινήσεις που μπορούνε να γίνουν
# χρησιμοποιώντας 2 καταχωρητές ($v0 και $v1).
# Ο κάθε καταχωρητής είναι 32 bit, όταν το bit i (0 από αριστερά)
# είναι 1 σημαίνει ότι μπορεί στην θέση i της σκακιέρας να μπει βασίλισσα.
get_possible_moves:
	la $t0, board
	
	li $t4, 32
	
	li $t1, 0	# 32 πρώτες τιμές
	li $t2, 0	# 32 επόμενες τιμές
	
	get_possible_moves_loop1:
		lb $t3, 0($t0)
		
		sll $t1, $t1, 1
		add $t1, $t1, $t3
		
		addi $t0, $t0, 1
		addi $t4, $t4, -1
		bnez $t4, get_possible_moves_loop1
	
	li $t4, 32
	
	get_possible_moves_loop2:
		lb $t3, 0($t0)
		
		sll $t2, $t2, 1
		add $t2, $t2, $t3
		
		addi $t0, $t0, 1
		addi $t4, $t4, -1
		bnez $t4, get_possible_moves_loop2
	
	move $v0, $t1
	move $v1, $t2
	jr $ra
