# MIPS_Projects

This is a collection of MIPS assembly mini-projects.

*  Implementation of [Dijkstra algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm).
*  Solution of [Eight queen problem](https://en.wikipedia.org/wiki/Eight_queens_puzzle) using brute force technique.
*  [Binary Search Tree](https://en.wikipedia.org/wiki/Binary_search_tree) implementation.
*  Get Nth [Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number).
*  Find [Factorial](https://en.wikipedia.org/wiki/Factorial) of N.
*  [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck) interpreter.

[Mars simulation](https://courses.missouristate.edu/KenVollmar/MARS/index.htm)
