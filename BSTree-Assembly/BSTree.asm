.text
.globl __start
__start:

	li $a0, 120
	li $a1, 3
	jal Tree
	move $s0, $v0

	li $a0, 97
	li $a1, 0
	move $a2, $s0
	jal Tree

	li $a0, 107
	li $a1, 0
	move $a2, $s0
	jal Tree

	li $a0, 124
	li $a1, 0
	move $a2, $s0
	jal Tree

	li $a0, 121
	li $a1, 0
	move $a2, $s0
	jal Tree

	li $a0, 125
	li $a1, 0
	move $a2, $s0
	jal Tree

	li $a0, 10
	li $a1, 1
	move $a2, $s0
	jal Tree


	li $v0, 10
	syscall

# Tree($a0, $a1, (optional) $a2): $a0 = Data of node, $a1 = Operation, $a2 = Address of root.
Tree:
	li $t0, 0
	beq $a1, $t0, Insert

	li $t0, 1
	beq $a1, $t0, Delete
	
	li $t0, 2
	beq $a1, $t0, Search

	li $t0, 3
	beq $a1, $t0, Initialize

	# Insert($a0, $a2): $a0 = Data of node, $a2 = Address of root.
	Insert:
		addi $sp, $sp, -4
		sw $ra, 0($sp)
		jal CreateNode
		lw $ra, 0($sp)
		addi $sp, $sp, 4

		li $t0, -1
		beq $v0, $t0, node_failed

		Find_position:
			lw $t1, 0($a2)
			slt $t2, $a0, $t1

			bne $t2, $zero, go_to_left
			j go_to_right

			go_to_left:
				lw $t1, 4($a2)
				li $t2, -1
				
				beq $t1, $t2, place_on_left
				move $a2, $t1
				j Find_position

				place_on_left:
					sw $v0, 4($a2)
					j  $ra

			go_to_right:
				lw $t1, 8($a2)
				li $t2, -1
				
				beq $t1, $t2, place_on_right
				move $a2, $t1
				j Find_position

				place_on_right:
					sw $v0, 8($a2)
					j  $ra

		node_failed:
			la $a0, NodeFailed
			li $v0, 4
			syscall
			j $ra

	#Delete($a0, $a2): $a0 = Data of node, $a2 = Address of root.
	Delete:
		addi $t0, $zero, -1
		beq $a2, $t1, is_empty

		addi $sp, $sp, -4
		sw $ra, 0($sp)
		jal Find_Node
		lw $ra, 0($sp)
		addi $sp, $sp, 4

		addi $t0, $zero, -1
		beq $v0, $t0, not_in_tree

		lw $t0, 4($v0)
		lw $t1, 8($v0)
		beq $t0, $t1, is_leaf
		j $ra

		is_leaf:
			beq $v1, $zero, is_root
			j delete_leaf

			is_root:
				sw $zero, 0($v0)
				sw $zero, 4($v0)
				sw $zero, 8($v0)
				j $ra

		delete_leaf:
			addi $t0, $zero, -1
			sw $t0, 0($v1)
			sw $zero, 0($v0)
			sw $zero, 4($v0)
			sw $zero, 8($v0)


		not_in_tree:
			la $a0, NotInTree
			li $v0, 4
			syscall
			j $ra
		is_empty:
			li $v0, 4
			la $a0, isEmpty
			syscall
			j $ra

	# $v0 = 0 (if there is not in tree) else return 1.
	# Search($a0, $a2): $a0 = Data of node, $a2 = Address of root.
	Search:
		addi $sp, $sp, -4
		sw $ra, 0($sp)
		jal Find_Node
		lw $ra, 0($sp)
		addi $sp, $sp, 4

		addi $t0, $zero, -1
		beq $v0, $t0, Search_False
		j Search_True

		Search_True:
			addi $v0, $zero, 1
			j $ra
		Search_False:
			addi $v0, $zero, 0

	# $v0 = -1 (if creation of binary tree fails) else the address of the root.
	# Initialize($a0): $a0 = Data of node.
	Initialize:
		addi $sp, $sp, -4
		sw $ra, 0($sp)
		jal CreateNode
		lw $ra, 0($sp)
		addi $sp, $sp, 4

		li $t0, -1
		beq $v0, $t0, init_failed
		
		j $ra
		init_failed:
			la $a0, InitFailed
			li $v0, 4
			syscall
			li $v0, -1
			j $ra

	# $v0 = -1 (if node creation failed) else the address of node.
	# CreateNode($a0). $a0 = data of the node.
	CreateNode:
		addi $sp, $sp, -4
		sw $ra, 0($sp)
		jal Find_Space
		lw $ra, 0($sp)
		addi $sp, $sp, 4

		li $t0, -1
		beq $v0, $t0, out_of_mem

		li $t0, -1
		sw $a0, 0($v0)
		sw $t0, 4($v0)
		sw $t0, 8($v0)
		j $ra

		out_of_mem: j $ra

	# $v0 = -1 (if there is no space in memory) else the address that will store the node.
	# Find_Space()
	Find_Space:
		li $t0, 0xFFFFFFC
		li $t3, 0x10040000

		Find_Space_Loop:
			beq $t0, $t3, Find_Space_Exit

			addi $t0, 4
			lw $t1, 0($t0)
			lw $t2, 4($t0)
			lw $t3, 8($t0)

			bne $t1, $zero, Find_Space_Loop
			bne $t2, $zero, Find_Space_Loop
			bne $t3, $zero, Find_Space_Loop

			move $v0, $t0
			j $ra
		Find_Space_Exit:
			addi $v0, $zero, -1
			j $ra

	# $v0 = -1 (if $a0 is not in tree) else the address of $a0 node, $v1 = Address of left or right link (if exist) else 0.
	#Find_Node($a0, $a2): $a0 = Data of node, $a2 = Address of root.
	Find_Node:
		move $t1, $zero
		Find_NodeL:
			lw $t0, 0($a2)
		
			beq $t0, $a0, Find_True

			lw $t2, 4($a2)
			lw $t3, 8($a2)
			
			li $t4, -1
			beq $t2, $t3, Find_False
			beq $t2, $t4, Find_right
			beq $t3, $t4, Find_left

			slt $t1, $t0, $a0
			beq $t1, $zero, Find_left
			j Find_right

			Find_True:
				move $v0, $a2
				move $v1, $t1
				jr $ra
			Find_False:
				addi $v0, $zero, -1
				addi $v1, $zero, 0
				jr $ra
			Find_left:
				addi $t1, $a2, 4
				lw $a2, 4($a2)
				j Find_NodeL
			Find_right:
				addi $t1, $a2, 8
				lw $a2, 8($a2)
				j Find_NodeL



.data
NodeFailed: .ascii "Node creation failed, out of memory.\n"
InitFailed: .ascii "Initialization of binary tree failed\n"
isEmpty:	.ascii "Tree is empty.\n"
NotInTree:	.ascii "The element is not present in the tree\n"