.data
	file: .asciiz "/home/george/Dropbox/Projects/MIPS/brainfuck/main.txt"
	code: .space 2048
	array: .space 30000
.text
.globl __start

__start:
	
	#data pointer
	li $s0, 0

	#instruction pointer
	li $s1, 0

	#Open source code
	la $a0, file
	li $a1, 0
	li $a2, 0
	li $v0, 13
	syscall

	add $t0, $zero, $v0

	#Read source code
	add $a0, $zero, $t0
	la $a1, code
	li $a2, 2048
	li $v0, 14
	syscall

	main_loop:
		lb $t0, code($s1)
		beq $t0, $zero, exit

		beq $t0, '>', inc_dp
		beq $t0, '<', dec_dp
		beq $t0, '+', inc_byte
		beq $t0, '-', dec_byte
		beq $t0, '.', out_byte
		beq $t0, ',', in_byte
		beq $t0, '[', start_loop
		beq $t0, ']', end_loop

		return:
			addi $s1, $s1, 1
			j main_loop

	#Exit
	exit:
		li $v0, 10
		syscall

#Execute >
inc_dp:
	addi $s0, $s0, 1
	j return
#Execute <
dec_dp:
	addi $s0, $s0, -1
	j return
#Execute +
inc_byte:
	lb $t0, array($s0)
	addi $t0, $t0, 1
	sb $t0, array($s0)
	j return
#Execute -
dec_byte:
	lb $t0, array($s0)
	addi $t0, $t0, -1
	sb $t0, array($s0)
	j return
#Execute .
out_byte:
	lb $a0, array($s0)
	li $v0, 11
	syscall
	j return
#Execute ,
in_byte:
	li $v0, 12
	sb $v0, array($s0)
	j return
#Execute [
start_loop:
	lb $t0, array($s0)
	li $t1, 1
	beq $t0, $zero, jump_forward
	addi $sp, $sp, -4
	sb $s1, 0($sp)
	j return

	jump_forward:
		addi $s1, $s1, 1
		lb $t0, code($s1)
		beq $t0, '[', add_one
		beq $t0, ']', sub_one
		j jump_forward

		add_one:
			addi $t1, $t1, 1
			j jump_forward

		sub_one:
			addi $t1, $t1, -1
			beq $t1, $zero, return
			j jump_forward
#Execute ]
end_loop:
	lb $t0, array($s0)
	bne $t0, $zero, jump_back
	j return

	jump_back:
		lb $t0, 0($sp)
		addi $sp, $sp, 4
		add $s1, $t0, $zero
		j return
