.text
.globl __start

__start:

	la $t0, N
	lw $s0, 0($t0)
	#jal Fibo

	li $s1, 1

Loop:
	move $a0, $s1
	jal Fibo

	move $a0, $v0
	li $v0, 1
	syscall

	#move $s0, $a0
	la $a0, endl
	li $v0, 4
	syscall
	#move $a0, $s0

	addi $s1, $s1, 1
	slt $t0, $s1, $s0
	beq $t0,$zero,end
	j Loop

end:
	li $v0, 10
	syscall

Fibo:
	move $v0, $zero
F:
	slti $t0, $a0, 3
	bne $t0, $zero, Exit

	addi $sp, $sp, -8
	sw $a0, 0($sp)
	sw $ra, 4($sp)
	addi $a0, $a0, -1
	jal F

	lw $a0, 0($sp)
	addi $sp, $sp, 4
	addi $a0, $a0, -2
	jal F

	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra

	Exit:
		addi $v0, $v0, 1
		jr $ra

.data
N: .word 11
endl: .asciiz "\n"