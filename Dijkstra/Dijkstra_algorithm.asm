.data
	# TODO: parse the graph from a file ... or not TODO.
	# Just hard code.
	graph:
		.asciiz "A"
		.align 2
		.byte 'B', 1
		.align 2
		.byte 'C', 7
		.align 2
		.byte 'D', 2
		.word 0xFFFFFFFF
		
		.asciiz "B"
		.align 2
		.byte 'A', 1
		.align 2
		.byte 'C', 1
		.align 2
		.byte 'E', 6
		.word 0xFFFFFFFF
		
		.asciiz "C"
		.align 2
		.byte 'A', 7
		.align 2
		.byte 'B', 1
		.align 2
		.byte 'E', 4
		.align 2
		.byte 'F', 8
		.word 0xFFFFFFFF
		
		.asciiz "D"
		.align 2
		.byte 'A', 2
		.align 2
		.byte 'F', 8
		.word 0xFFFFFFFF
		
		.asciiz "E"
		.align 2
		.byte 'B', 6
		.align 2
		.byte 'C', 4
		.align 2
		.byte 'G', 3
		.word 0xFFFFFFFF
		
		.asciiz "F"
		.align 2
		.byte 'C', 8
		.align 2
		.byte 'D', 8
		.align 2
		.byte 'G', 1
		.word 0xFFFFFFFF
		
		.asciiz "G"
		.align 2
		.byte 'E', 3
		.align 2
		.byte 'F', 1
		.word 0xFFFFFFFF
		
		.word 0xEEEEEEEE	# End of graph

	start_node: .asciiz "A"
	.align 2
	end_node: .asciiz "G"
	
	node_end_symbol: .word 0xFFFFFFFF
	graph_end_symbol: .word 0xEEEEEEEE
	infinity: .word 0x7FFFFFFF
	null: .word 0xDEADC0DE
	
	distances: .space 1024
	unvisited_nodes: .space 1024
	visited_nodes: .space 1024
	prevs:
		.align 2
		.asciiz "A"
		.space 4
		.align 2
		.asciiz "B"
		.space 4
		.align 2
		.asciiz "C"
		.space 4
		.align 2
		.asciiz "D"
		.space 4
		.align 2
		.asciiz "E"
		.space 4
		.align 2
		.asciiz "F"
		.space 4
		.align 2
		.asciiz "G"
		.space 4
	
.text

.macro return (%v)
li $a0, %v
li $v0, 17
syscall
.end_macro

# ===== Graph Functions =====
.macro get_address_of_node (%node, %return)
	la $t0, graph
	lw $t2, node_end_symbol
	lw $t3, graph_end_symbol
	loop:
		lw $t1, 0($t0)
		beq $t1, %node, return_address
		beq $t1, $t3, no_return
		addi $t0, $t0, 4
		j loop
	no_return:
		lw %return, null
		j exit
	return_address:
		move %return, $t0
	exit:
.end_macro
# ===========================

# ===== Unvisited Functions =====
.macro remove_unvisited (%node)
	la $t0, unvisited_nodes
	find_node_loop:
		lw $t1, 0($t0)
		addi $t0, $t0, 4
		bne $t1, %node, find_node_loop
	shift_list_loop:
		lw $t1, 0($t0)
		sw $t1, -4($t0)
		addi $t0, $t0, 4
		bnez $t1, shift_list_loop
.end_macro

.macro add_unvisited (%node)
	la $t0, unvisited_nodes
	loop:
		lw $t1, 0($t0)
		addi $t0, $t0, 4
		bnez $t1, loop
	sw %node, -4($t0)
.end_macro

.macro node_is_unvisited (%node, %return)
	la $t0, unvisited_nodes
	loop:
		addi $t0, $t0, 4
		lw $t1, -4($t0)
		beqz $t1, not_exists
		beq $t1, %node, exists
		j loop
	not_exists:
		li %return, 0
		j exit
	exists:
		li %return, 1
		j exit
	exit:		
.end_macro

.macro get_smallest_unvisited (%return)
	la $t0, unvisited_nodes
	li $t2, -1
	lw $t4, infinity
	loop:
		addi $t0, $t0, 4
		lw $t1, -4($t0)
		beqz $t1, return_min
		get_distance $t1, $t3
		blt $t3, $t4, set_new_min
		j loop
	return_min:
		move %return, $t2
		j exit
	set_new_min:
		move $t4, $t3
		move $t2, $t1
		j loop
	exit:
.end_macro
# ===================================

# ===== Visited Functions =====
.macro add_visited (%node)
.end_macro
# =================================

# ===== Prevs Functions =====
.macro update_prev (%node, %value)
	la $t0, prevs
	loop:
		lw $t1, 0($t0)
		beq $t1, %node, change
		addi $t0, $t0, 8
		j loop
		# TODO: check for end of table
	change:
		sw %value, 4($t0)
.end_macro

.macro get_prev (%node, %return)
	la $t0, prevs
	loop:
		lw $t1, 0($t0)
		beq $t1, %node, return_node
		addi $t0, $t0, 8
		j loop
	return_node:
		lw %return, 4($t0)
.end_macro
# =================================

# ===== Distances Functions =====
.macro update_distance (%node, %value)
	la $t0, distances
	loop:
		lw $t1, 0($t0)
		beq $t1, %node, change
		addi $t0, $t0, 8
		j loop
		# TODO: check for end of table
	change:
		sw %value, 4($t0)
.end_macro

.macro add_distance (%node, %value)
	la $t0, distances
	loop:
		lw $t1, 0($t0)
		beqz $t1, add_item
		addi $t0, $t0, 8
		j loop
	add_item:
		sw %node, 0($t0)
		sw %value, 4($t0)
.end_macro

.macro get_distance (%node, %return)
	la $t5, distances
	loop:
		addi $t5, $t5, 8
		lw $t6, -8($t5)
		bne $t6, %node, loop
		lw %return, -4($t5)
.end_macro
# ===================================

main:
	
	la $s1, graph
	lw $s4, infinity
	
	
	init_unvisited_loop:
		lw $s2, 0($s1)
		lw $s3, graph_end_symbol
		beq $s2, $s3, init_unvisited_exit
		# Mark all nodes unvisited.
		add_unvisited $s2
		# Create distances table and assign to every node a tentative distance value.
		add_distance $s2, $s4
		next_node_loop:
			addi $s1, $s1, 4
			lw $s2, 0($s1)
			lw $s3, node_end_symbol
			bne $s2, $s3, next_node_loop
			addi $s1, $s1, 4
			j init_unvisited_loop
	init_unvisited_exit:
	
	# set it to zero for our initial node.
	lw $s1, start_node
	update_distance $s1, $zero
	
	
	# For the current node, consider all of its unvisited neighbors
	# and calculate their tentative distances through the current node
	# $s0: current node
	# $s1: neighbor address
	# $s2: neighbor node
	# $s3: edge cost
	lw $s0, start_node
	get_address_of_node $s0, $s1
	
	loop_neighbors:
		
		# Get next neighbor
		addi $s1, $s1, 4
		lw $s2, 0($s1)
		
		# Check if neighbor exists
		lw $t0, node_end_symbol
		beq $s2, $t0, no_more_neighbors
		
		# Check if its unvisited
		srl $s3, $s2, 8
		andi $s2, $s2, 0x000000FF
		node_is_unvisited $s2, $s7
		beqz $s7, loop_neighbors
		
		# Calculate the tentative distance
		get_distance $s0, $s5
		add $s4, $s3, $s5 # Tentative distance through the current node.
		
		get_distance $s2, $s5
		
		# Compare the newly calculated tentative distance
		# to the current assigned value and assign the smaller one.
		blt $s4, $s5, set_new_path
		j loop_neighbors
	
	set_new_path:
		update_prev $s2, $s0
		update_distance $s2, $s4
		j loop_neighbors
	no_more_neighbors:
	
	# Remove current node from unvisited list.
	remove_unvisited $s0
	
	# Get unvisited node with the smallest tentative distance and set it as current.
	get_smallest_unvisited $s0
	li $t0, -1
	beq $s0, $t0, no_more_unvisited	
	get_address_of_node $s0, $s1
	
	j loop_neighbors	
	
	no_more_unvisited:
	
	# Print Solution path.
	lw $s0, end_node
	print_solution_loop:
			
		get_address_of_node $s0, $a0
		li $v0, 4
		syscall
		
		get_prev $s0, $s1
		
		beqz $s1, end_of_path
		move $s0, $s1
		j print_solution_loop
	
	end_of_path:
	
	return 0
