.data
	number_of_nodes: .asciiz  "How many nodes?"
	node_name: .asciiz "Node name"
	number_of_node_edges: .asciiz "Number of edges in node"
	edge_of_nodeX: .asciiz "Edge of node "
	edge_cost: .asciiz "Cost of edge"
	empty_str: .asciiz ""
	
	node_end: .word 0xFFFFFFF
	graph_end: .word 0xEEEEEEEE

	buffer64: .space 64
	
	graph: .space 1024

.text

# $s1: used to traverse the struct (graph)

main:

create_graph:
	la $v0, 51
	la $a0, number_of_nodes
	syscall
	move $s0, $a0
		
	la $s1, graph

	create_nodes_loop:
		li $v0, 54
		la $a0, node_name
		move $a1, $s1
		li $a2, 2
		syscall
		
		li $v0, 51
		la $a0, number_of_node_edges
		syscall
		
		sll $a0, $a0, 2
		addi $a0, $a0, 4
		add $s1, $s1, $a0
		lw $t0, node_end
		sw $t0, 0($s1)
		
		addi $s1, $s1, 4
		addi $s0, $s0, -1
		bnez $s0, create_nodes_loop
	
	lw $t0, graph_end
	sw $t0, 0($s1)
	
	addi $sp, $sp, 4
	sw $ra, 0($sp)

	### DEbUG ###
	#li $v0, 10
	#syscall
	### DEbUG ###
					
	la $s1, graph
	move $s2, $s1
	addi $s1, $s1, 4
	create_edges_loop:

		lw $t0, 0($s1)
		lw $t1, node_end
		beq $t0, $t1, next_node

		la $a0, edge_of_nodeX
		la $a1, buffer64
		la $a2, ($s2)
		jal concatenate_strings

		li $v0, 54
		la $a0, buffer64
		la $a1, 0($s1)
		li $a2, 2
		syscall

		li $v0, 51
		la $a0, edge_cost
		syscall

		
		addi $s1, $s1, 4
		j create_edges_loop
		
		next_node:
			addi $s2, $s1, 4
			addi $s1, $s1, 8

			lw $t1, graph_end
			beq $t0, $t1, end_of_graph
			
			j create_edges_loop
		end_of_graph:


	lw $ra, 0($sp)
	addi $sp, $sp, -4


# $a0: address of string 1
# $a1: address of result buffer
# $a2: address of string 2
concatenate_strings:
	
	addi $sp, $sp, 4
	sw $ra, 0($sp)

	# Copy first string to result buffer
	jal strcopier

	# Concatenate second string on result buffer
	move $a0, $a2
	or $a1, $v0, $zero
	jal strcopier

	lw $ra, 0($sp)
	addi $sp, $sp, -4
	jr $ra

	# String copier function
	strcopier:
		or $t0, $a0, $zero # Source
		or $t1, $a1, $zero # Destination

		loop:
			lb $t2, 0($t0)
			beq $t2, $zero, end
			addiu $t0, $t0, 1
			sb $t2, 0($t1)
			addiu $t1, $t1, 1
			b loop

	end:
		or $v0, $t1, $zero # Return last position on result buffer
		jr $ra
