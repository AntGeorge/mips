.text
.globl __start
__start:
	
	addi $a0, $zero, 13
	jal P

	add $a0, $v0, $zero
	li $v0, 1
	syscall


	li $v0,10
	syscall

P:
	beq $a0, $zero, Exit
	
	addi $sp, $sp, -8
	sw $ra, 4($sp)
	sw $a0, 0($sp)

	addi $a0, $a0, -1
	jal P

	lw $ra, 4($sp)
	lw $t0, 0($sp)
	addi $sp, $sp, 8

	mul $v0, $v0, $t0
	jr $ra

	Exit:
		addi $v0, $zero, 1
		jr $ra
